from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Game, Message

admin.site.register(Game)
admin.site.register(Message)


# Register your models here.
