#!/usr/bin/env python3
import json
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import Message, Game
from django.core.exceptions import ObjectDoesNotExist
from channels.db import database_sync_to_async


class GameConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'room_%s' % self.room_name
        await self.GetorCreateGame()
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, code):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def GetorCreateGame(self):
        try:
            self.game = await database_sync_to_async(Game.objects.get)(name=self.room_name)
        except ObjectDoesNotExist:
            self.game = await database_sync_to_async(Game.objects.create)(name=self.room_name)
            await database_sync_to_async(self.game.save)()

    async def receive(self, text_data):
        obj = json.loads(text_data)
        if(obj['action'] == 'request'):
            # Do backlog
            await self.backlogRequest()
        else:
            msg = Message()
            msg.content = text_data
            msg.game = self.game
            await database_sync_to_async(msg.save)()
            await self.channel_layer.group_send(
                self.room_group_name,
                {
                    'type': 'msg',
                    'message': text_data,
                }
            )

    async def backlogRequest(self):
        backlog = await database_sync_to_async(self.game.messages)()
        messages = []
        for b in backlog:
            messages.append(b.content)
        for pos in range(0, len(messages), 300):
            await self.send(text_data=json.dumps(messages[pos: pos+300]))
        #await self.send(text_data=json.dumps(messages))

    async def msg(self, event):
        await self.send(text_data=event['message'])
