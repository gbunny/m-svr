from django.test import TestCase
from channels.testing import WebsocketCommunicator
from .routing import websocket_urlpatterns
from channels.routing import URLRouter
import json
# Create your tests here.

class MyTest(TestCase):
    app = URLRouter(websocket_urlpatterns)

    async def test_connection_test(self):
        communicator = WebsocketCommunicator(
            self.app,
            'ws/game/100/'
        )
        connected, subprotocol = await communicator.connect()
        self.assertTrue(connected)
        await communicator.disconnect()

    async def test_db_test(self):
        communicator = WebsocketCommunicator(
            self.app,
            'ws/game/100/'
        )
        connected, subprotocol = await communicator.connect()
        self.assertTrue(connected)
        msg = json.dumps({ 'action': 'image', 'payload': 'hello' })
        await communicator.send_to(text_data=msg)
        response = await communicator.receive_from()
        print(response)
        msg = json.dumps({ 'action': 'image', 'payload': 'hello2' })
        await communicator.send_to(text_data=msg)
        response = await communicator.receive_from()
        await communicator.disconnect()
        #Ask for log
        communicator = WebsocketCommunicator(
            self.app,
            'ws/game/100/'
        )
        connected, subprotocol = await communicator.connect()
        self.assertTrue(connected)
        msg = json.dumps({ 'action': 'request' })
        await communicator.send_to(text_data=msg)
        response = await communicator.receive_from()
        print(response)
        await communicator.disconnect()
